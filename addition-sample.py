from __future__ import print_function
import torch

x = torch.rand(5, 3)

print(x)

y = torch.rand(5, 3)

print(y)

print(x + y)

# 2nd syntax

print(torch.add(x,y))

# providing an output tensor as argument

result = torch.empty(5, 3)
torch.add(x, y, out=result)
print(result)

# adds x to y
y.add_(x)
print(y)

